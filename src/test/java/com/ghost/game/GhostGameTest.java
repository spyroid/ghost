package com.ghost.game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class GhostGameTest {

    GhostGame ghostGame;
    List<String> words;

    public GhostGameTest() {
        words = Arrays.asList("package", "pancake", "pistol", "pound");
    }

    @Before
    public void setUp() throws Exception {
        ghostGame = new GhostGame(words);
    }

    @Test
    public void play_getResponse_OK() throws Exception {
        GameResponse resp = ghostGame.play("p");

        Assert.assertNotNull(resp);
        Assert.assertTrue(resp.getWord().length() == 2);
        Assert.assertNull(resp.getFailMessage());
        Assert.assertTrue(resp.getState() == 0);

        resp = ghostGame.play("pisto");

        Assert.assertNotNull(resp);
        Assert.assertTrue(resp.getWord().length() == 6);
        Assert.assertNull(resp.getFailMessage());
        Assert.assertTrue(resp.getState() == 1);

        resp = ghostGame.play("pound");

        Assert.assertNotNull(resp);
        Assert.assertTrue(resp.getWord().length() == 5);
        Assert.assertNotNull(resp.getFailMessage());
        Assert.assertTrue(resp.getState() == -1);

        resp = ghostGame.play("c");

        Assert.assertNotNull(resp);
        Assert.assertTrue(resp.getWord().length() == 1);
        Assert.assertNotNull(resp.getFailMessage());
        Assert.assertTrue(resp.getState() == -1);
    }
}
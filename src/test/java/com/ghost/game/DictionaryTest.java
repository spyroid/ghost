package com.ghost.game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class DictionaryTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    Dictionary dict;
    private List<String> words;

    public DictionaryTest() {
        words = Arrays.asList("packageeeeeee", "packabeeeee", "abba", "packooge", "packoge", "arab");
    }

    @Before
    public void setUp() throws Exception {
        dict = new Dictionary(words);
    }

    @Test
    public void testIsCompleteWord() throws Exception {
        Assert.assertTrue(dict.getWordMapping("abba") == 0);
        Assert.assertTrue(dict.getWordMapping("abb") == -1);
        Assert.assertTrue(dict.getWordMapping("pack") == -1);
        Assert.assertTrue(dict.getWordMapping("abbat") == 1);
    }

    @Test
    public void getNextWord_OK() throws Exception {
        String word = dict.getNextWord("packa");
        Assert.assertTrue(word.length() == 6);
        Assert.assertTrue(word.contains("b") || word.contains("g"));

        word = dict.getNextWord("abb");
        Assert.assertTrue(word.length() == 5);
        Assert.assertTrue(word.equals("abba-"));

    }
}
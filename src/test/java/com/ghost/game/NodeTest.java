package com.ghost.game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NodeTest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void nodeConstruct_create_OK() {
        Node node = new Node("cat");

        Assert.assertNotNull(node);
        Assert.assertTrue(node.getChar() == 'c');
        Assert.assertTrue(node.getChildren() != null);
        Assert.assertTrue(node.getChildren().size() == 1);

        node = node.getChildren().get('a');

        Assert.assertNotNull(node);
        Assert.assertTrue(node.getChar() == 'a');
        Assert.assertTrue(node.getChildren() != null);
        Assert.assertTrue(node.getChildren().size() == 1);

        node = node.getChildren().get('t');

        Assert.assertNotNull(node);
        Assert.assertTrue(node.getChar() == 't');
        Assert.assertTrue(node.getChildren() == null);

    }

    @Test
    public void add_addWord_OK() {
        Node node = new Node("cat");
        node.add("car");

        Assert.assertTrue(node.getChildren().size() == 1);

        node = node.getChildren().get('a');

        Assert.assertNotNull(node);
        Assert.assertTrue(node.getChar() == 'a');
        Assert.assertTrue(node.getChildren() != null);
        Assert.assertTrue(node.getChildren().size() == 2);

        node = node.getChildren().get('r');

        Assert.assertNotNull(node);
        Assert.assertTrue(node.getChar() == 'r');
        Assert.assertTrue(node.getChildren() == null);

    }


    @Test
    public void getWordMapping_checkMapping_OK() {
        Node node = new Node("package");

        Assert.assertTrue(node.getWordMapping("pack") == -1);
        Assert.assertTrue(node.getWordMapping("package") == 0);
        Assert.assertTrue(node.getWordMapping("packaged") == 1);
        Assert.assertTrue(node.getWordMapping("cat") == 1);
    }

    @Test
    public void findNode_getNode_OK() {
        Node node = new Node("package");

        Node n = node.findNode("pack");

        Assert.assertTrue(n.getChar() == 'k');
        Assert.assertTrue(n.getChildren().size() == 1);


    }


    @Test
    public void hasOddChildren_getBoolean_OK() {
        Node node = new Node("packaged");
        Assert.assertTrue(node.hasOddChildren());

        node = new Node("package");
        Assert.assertFalse(node.hasOddChildren());

        node.add("packatee");
        Assert.assertTrue(node.hasOddChildren());
    }

    @Test
    public void getLongestPath_getInt_OK() {
        Node node = new Node("packaged");
        node.add("page");
        Assert.assertTrue(node.getLongestPath() == "packaged".length() - 1);
        node.add("pandorraworld");
        Assert.assertTrue(node.getLongestPath() == "pandorraworld".length() - 1);
    }


}
package com.ghost.game;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class GhostGame {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private Dictionary dictionary;

    /**
     * Constructor which accepts array of words to initialize
     * @param words List
     */
    public GhostGame(List<String> words) {

        dictionary = new Dictionary(words);
        logger.info("Game initialized with " + words.size() + " words");
    }

    /**
     * Play the  game
     * @param word String
     * @return GameResponse
     */
    public GameResponse play(String word) {

        String answer = null;

        int wordStatus = dictionary.getWordMapping(word);

        if ( wordStatus > 0) { // entered word in not exists
            return new GameResponse(word, -1, "Word is not exists");
        } else if (wordStatus == 0) { // entered word is matched
            return new GameResponse(word, -1, "Word is fully matched");
        } else {
            answer = dictionary.getNextWord(word);
            if (answer == null) {
                return new GameResponse(word, 1, null);
            } else if (answer.contains("-")) {
                return new GameResponse(answer.substring(0, answer.length() - 1), 1, null);
            }
        }

        return new GameResponse(answer, 0, null);
    }
}

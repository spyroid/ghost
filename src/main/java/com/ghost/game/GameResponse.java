package com.ghost.game;

/**
 * Describes response from game
 */
public class GameResponse {

    /**
     * State of game: 1: user won, 0: continue the game, -1: user loose
     */
    private Integer state = null;
    /**
     * Message with details of fail
     */
    private String failMessage;
    /**
     * Response word
     */
    private String word;


    public GameResponse(String word, int state, String failMessage) {
        setState(state);
        setWord(word);
        setFailMessage(failMessage);
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getFailMessage() {
        return failMessage;
    }

    public void setFailMessage(String failMessage) {
        this.failMessage = failMessage;
    }
}

package com.ghost.game;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents one element in a tree
 */
public class Node {

    /**
     * Holds current letter
     */
    private Character ch;
    /**
     * Children nodes
     */
    private Map<Character, Node> children;


    /**
     * Constructor of the node. Recursively builds child nodes
     * @param word String
     */
    public Node(String word) {
        if (word.length() > 0) {
            ch = word.charAt(0);
        }

        if (word.length() > 1) {
            children = new HashMap<>();
            children.put(word.charAt(1), new Node(word.substring(1)));
        }
    }


    /**
     * Recursively add a word into child nodes
     * @param word String
     */
    public void add(String word) {
        if (word.charAt(0) == ch) {
            if (children != null) {
                if (word.length() == 1) {
                    children = null;
                } else {
                    Node nextNode = children.get(word.charAt(1));

                    if (nextNode == null) {
                        children.put(word.charAt(1), new Node(word.substring(1)));
                    } else {
                        nextNode.add(word.substring(1));
                    }
                }
            } else {
                if (word.length() > 1) {
                    children = new HashMap<>();
                    children.put(word.charAt(1), new Node(word.substring(1)));
                }
            }
        }
    }

    /**
     * Return how specified word mapped into tree. Works recursively.
     * @param word String
     * @return int
     * 0: word fit exactly,
     * -1: word is less then stored path
     * 1: word is longer then stored path
     */
    public int getWordMapping(String word) {

        if (isLeaf())  {
            if (word.isEmpty()) {
                return -1;
            } else if (word.length() > 1) {
                return 1;
            } else {
                return getChar().equals(word.charAt(0)) ? 0 : 1;
            }
        } else {
            if (word.isEmpty()) {
                return -1;
            } else if (word.length() > 1) {
                if (!children.containsKey(word.charAt(1))) {
                    return 1;
                }
                return children.get(word.charAt(1)).getWordMapping(word.substring(1));
            }
        }

        return -1;
    }

    /**
     * Find the last node in the path of specified word
     * @param word String
     * @return Node
     */
    public Node findNode(String word) {
        if (word.length() > 1) {
            return children.get(word.charAt(1)).findNode((word.substring(1)));
        }

        return this;
    }

    /**
     * Returns if current node if the leaf of the tree
     * @return boolean
     */
    public boolean isLeaf() {
        return children == null;
    }

    /**
     * Returns letter specified the current node
     * @return Character
     */
    public Character getChar() {
        return ch;
    }

    /**
     * Returns children object
     * @return Map
     */
    public Map<Character, Node> getChildren() {
        return children;
    }

    /**
     * Checks of child sub-tree has path with odd length
     * @return boolean
     */
    public boolean hasOddChildren() {
        return hasOddChildren(1);
    }

    /**
     * Recursively calculates if odd length exists
     * @param counter int
     * @return boolean
     */
    private boolean hasOddChildren(int counter) {

        if (isLeaf()) {
            return counter % 2 == 0;
        }

        for (Node n: children.values()) {
            boolean v = n.hasOddChildren(counter + 1);

            if (v) {
                return true;
            }
        }

        return false;
    }

    /**
     * Calculates longest path
     * @return int
     */
    public int getLongestPath() {
        return calculateLongestPath(0, 1);
    }

    /**
     * Recursively calculates the longest path
     * @param counter int
     * @param max int
     */
    private int calculateLongestPath(int counter, Integer max) {

        if (isLeaf()) {
            return Math.max(counter, max);
        }

        for (Node n: children.values()) {
            int l = n.calculateLongestPath(counter + 1, max);

            if (l > max) {
                max = l;
            }
        }

        return max;
    }

}

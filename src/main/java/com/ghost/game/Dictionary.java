package com.ghost.game;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Dictionary {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * Holds all loaded words a tree
     */
    private Map<Character, Node> tree = new HashMap<>();

    /**
     * Random generator
     */
    private Random rand = new Random();


    /**
     * Constructor which initializing objects with specified array of words
     * @param words List<String>
     */
    public Dictionary(List<String> words) {

        for(int i = 0; i < words.size(); i++) {
            add(words.get(i));

            if (i % 20000 == 0 && i != 0) {
                logger.info("Loadded " + i + " words");
            }
        }

    }

    /**
     * Add one word into tree
     * @param word String
     */
    private void add(String word) {

        Node node = tree.get(word.charAt(0));

        if (node == null) {
            tree.put(word.charAt(0), new Node(word));
        } else {
            node.add(word);
        }

    }

    /**
     * Return how specified word mapped into tree
     * @param word String
     * @return int
     * 0: word fit exactly,
     * -1: word is less then stored path
     * 1: word is longer then stored path
     */
    public int getWordMapping(String word) {

        if (!tree.containsKey(word.charAt(0))) {
            return 1;
        }

        Node node = tree.get(word.charAt(0));
        return node.getWordMapping(word);
    }

    /**
     * Finds next most suitable word for win
     * @param word String
     * @return String
     * Result string consists of specified word plus most suitable character. If result is null no word is found
     * If result contains at the end '-' then character before '-' is take from leaf node
     */
    public String getNextWord(String word) {

        Node root = tree.get(word.charAt(0));
        if (root == null) return null;

        Node node = root.findNode(word);
        if (node == null || node.isLeaf()) return null;

        Character letter = null;

        // Try to find a winner char
        letter = findWinnerChar(node);
        if (letter != null) {
            return word + letter;
        }

        // Otherwise find char from a longest path
        String other = findCharFromLongestPath(node);
        if (other != null) {
            if (other.length() > 0) {
                return word + other;
            } else {
                return word + other.charAt(0);
            }
        }

        return null;
    }

    /**
     * Finding a character from longest path. Uses in case to make game longer
     * @param node Node
     * @return String
     * Result is the next available character. If character contains '-' that indicates that game is lost.
     */
    private String findCharFromLongestPath(Node node) {

        String letter = null;
        int longest = 0;
        boolean isLeaf = false;

        for (Node child: node.getChildren().values()) {
            int length = child.getLongestPath();
            if (length > longest) {
                longest = length;
                letter = "" + child.getChar();
                isLeaf = child.isLeaf();
            }
        }

        if (letter == null) {
            return null;
        } else {
            return isLeaf ? letter + "-" : letter;
        }

    }

    /**
     * Trying to find character from path with odd number of elements as they are winner candidates.
     * @param node Node
     * @return Character
     */
    private Character findWinnerChar(Node node) {

        List<Character> chars = new ArrayList<>();

        for (Node child: node.getChildren().values()) {
            if ( !child.isLeaf() && child.hasOddChildren() ) {
                chars.add(child.getChar());
            }
        }

        if (chars.isEmpty()) {
            return null;
        }

        int index = rand.nextInt(chars.size());

        return chars.get(index);
    }
}

package com.ghost.service;

import com.ghost.game.GameResponse;

public interface GameService {


    GameResponse play(String word);
}

package com.ghost.service;

import com.ghost.game.GameResponse;
import com.ghost.game.GhostGame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

@Component
public class GameServiceImpl implements GameService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private GhostGame ghostGame = null;

    @PostConstruct
    public void initGame() {

        try(InputStream is = getClass().getResourceAsStream("/words.txt")) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            List<String> words = new LinkedList<>();
            String line;

            while ((line = reader.readLine()) != null) {

                line = line.trim();
                if (line.isEmpty() || line.length() < 4) {
                    continue;
                }
                words.add(line);
            }

            ghostGame = new GhostGame(words);

        } catch (Exception e) {
            logger.error("Error reading resource file words.txt", e);
            throw new RuntimeException("Error reading resource file words.txt", e);
        }



    }

    @Override
    public GameResponse play(String word) {
        return ghostGame.play(word);
    }
}

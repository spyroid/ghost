package com.ghost.controller;

import com.ghost.game.GameResponse;
import com.ghost.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GameController {

    @Autowired
    private GameService gameService;

    @RequestMapping(value = "/game", produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<GameResponse> greeting(@RequestParam(value="word") String word) {

        GameResponse answer = gameService.play(word);

        return new ResponseEntity<GameResponse>(answer, HttpStatus.OK);
    }

}

'use strict';

(function () {

    'use strict';

	angular.module('ghostApp', []);

	angular.module('ghostApp').factory('ghostService', ['$http', function($http){

		function doGame(word, onSuccess, onError) {

			$http({method : 'GET', url : '/game?word=' + word})
            .success(function(data, status) {
                return onSuccess(data);
            })
            .error(function(data, status) {
                onError("Error " + status);
            });
		}

		return {
			doGame: doGame
		};
	}])

    angular.module('ghostApp').controller('ghostController', ['$scope', 'ghostService', '$timeout', function($scope, ghostService, $timeout){


    	function restart() {

	    	$scope.currentWord = '';
	    	$scope.inputChar = '';
	    	$scope.thinking = false;
			$scope.message = '';
			$scope.state = 0;

			$timeout(function(){
				$scope.el.focus();
			});				


    	}

    	restart();

    	var pattern = /[a-zA-Z]/;

    	$scope.changed = function() {
		
			if (pattern.test($scope.inputChar)) {

				$scope.thinking = true;

				$scope.currentWord += $scope.inputChar.toLowerCase();
				$scope.inputChar = '';

				ghostService.doGame($scope.currentWord, function(response){

					$scope.currentWord = response.word;
					$scope.thinking = false;
					$timeout(function () {
						$scope.el.focus();
					});

					$scope.state = response.state;

					if (response.state > 0) {
						$scope.message = "You won!!!";
					} else if (response.state < 0) {
						$scope.message = "You loose. " + response.failMessage;
					}


				}, function(err){
					$scope.thinking = false;
                    $scope.message = "Network error occurred. Probably server is down.";
                    $scope.state = -1 ;

					// error
				});


			} else {
				$scope.inputChar = '';
			}
	
			$scope.restart = restart;


    	};



    }]);


     angular.module('ghostApp').directive('letterField', [function(){
		return {
		    link: function(scope, element, attrs) {
		    	scope.el = element[0]; 
		    }
		  };

     }])

})();
